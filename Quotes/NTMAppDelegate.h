//
//  AppDelegate.h
//  Quotes
//
//  Created by Tagir Nafikov on 23.07.16.
//
//

#import <UIKit/UIKit.h>

@interface NTMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

