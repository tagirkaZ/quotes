//
//  ViewController.m
//  Quotes
//
//  Created by Tagir Nafikov on 23.07.16.
//
//

#import "NTMMainViewController.h"
#import "NTMCenteredLabelTableViewCell.h"
#import "NTMCommonTaskHelper.h"
#import "UIButton+HitArea.h"
#import "NTMQuotesManager.h"
#import "NTMUserInterfaceManager.h"



static const float NTMAnimationDuration = .4f;
static const float NTMAnimationDurationStep = .15f;
static const float NTMAnimationInitialDelay = .08f;
static const float NTMAnimationDelayStep = .025f;
static const float NTMFirstLabelDamping = .7f;
static const float NTMLabelsDampingStep = .075f;
static const float NTMTableViewDamping = 1.f;

static const float NTMMenuButtonHitTestInset = -15.f;



@interface NTMMainViewController () <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelsYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomSpaceConstraint;


@property (weak, nonatomic) IBOutlet UILabel *quoteTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *quoteValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *differenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, copy) NSArray *quotesArray;
@property (nonatomic) NSInteger selectedQuoteIndex;
@property (nonatomic) NSDateFormatter *onlyTimeDateFormatter;
@property (nonatomic) NTMQuotesBuilder *quotesBuilder;

@end



@implementation NTMMainViewController


#pragma mark - View's lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    self.tableView.hidden = NO;
    self.tableViewBottomSpaceConstraint.constant = -self.tableView.frame.size.height;
    
    self.menuButton.hitTestEdgeInsets = UIEdgeInsetsMake(NTMMenuButtonHitTestInset, NTMMenuButtonHitTestInset, NTMMenuButtonHitTestInset, NTMMenuButtonHitTestInset);
    
    self.timeLabel.text = @"";
    [self reloadLabels];
    
    [self updateQuotes];
}


#pragma mark - Subviews

- (void)reloadLabels {
    NTMQuote *quote = self.quotesArray[self.selectedQuoteIndex];
    
    self.quoteTypeLabel.attributedText = [[NSAttributedString alloc] initWithString:[self stringFromQuote:quote]
                                                               attributes:@{NSFontAttributeName : self.quoteTypeLabel.font,
                                                                            NSForegroundColorAttributeName : self.quoteTypeLabel.textColor/*,
                                                                            NSKernAttributeName : @(1.f)*/}];
    
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.minimumIntegerDigits = 1;
    numberFormatter.minimumFractionDigits = 3;
    numberFormatter.maximumFractionDigits = 3;
    
    self.quoteValueLabel.attributedText = [[NSAttributedString alloc] initWithString:[numberFormatter stringFromNumber:@(quote.value)]
                                                                          attributes:@{NSFontAttributeName : self.quoteValueLabel.font,
                                                                                       NSForegroundColorAttributeName : self.quoteValueLabel.textColor/*,
                                                                                       NSKernAttributeName : @(-4.f)*/}];
    
    NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
    parStyle.lineHeightMultiple = 1.21f;
    parStyle.alignment = NSTextAlignmentCenter;
    NTMQuotesDifference *difference = [self.quotesBuilder differenceSinceYesterdayForQuote:quote];
    NSString *differenceString = difference ? [self percentDifferenceTextForQuote:quote fromPercent:difference.percentDifference] : @"";
    self.differenceLabel.attributedText = [[NSAttributedString alloc] initWithString:differenceString
                                                                          attributes:@{NSFontAttributeName : self.differenceLabel.font,
                                                                                       NSForegroundColorAttributeName : [self percentDifferenceTextColorFromPercent:difference.percentDifference],
                                                                                       NSParagraphStyleAttributeName : parStyle}];
    
    [self.view layoutIfNeeded];
}

- (void)reloadTimeLabel {
    NSString *localizedString = [NSLocalizedString(@"UPDATED_AT", nil) uppercaseString];
    NSString *timeString = [self.onlyTimeDateFormatter stringFromDate:[NSDate date]];
    self.timeLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", localizedString, timeString]
                                                                        attributes:@{NSFontAttributeName : self.timeLabel.font,
                                                                                     NSForegroundColorAttributeName : self.timeLabel.textColor/*,
                                                                                     NSKernAttributeName : @(2.f)*/}];
}

- (void)showMenu {
    self.tableViewBottomSpaceConstraint.constant = 0;
    self.labelsYConstraint.constant = -66.f;
    
    [self animateConstraintChanges];
}

- (void)hideMenu {
    self.tableViewBottomSpaceConstraint.constant = -self.tableView.frame.size.height;
    self.labelsYConstraint.constant = 0;
    
    [self animateConstraintChanges];
}


#pragma mark - Model

- (void)updateQuotes {
    [[NTMQuotesManager sharedManager] getQuotesForLastTwoDaysWithCompletion:^(NSError *error, NTMQuotesBuilder *quotesBuilder) {
        if (error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR_TITLE", nil) message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else {
            self.quotesBuilder = quotesBuilder;
            NSMutableArray *newQuotes = [[NSMutableArray alloc] initWithCapacity:self.quotesArray.count];
            
            [self.quotesArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NTMQuote *quote = obj;
                NTMQuote *newQuote = [self.quotesBuilder quoteForFirstCurrencyType:quote.firstCurrency.type secondCurrencyType:quote.secondCurrency.type];
                [newQuotes addObject:newQuote];
            }];
            
            self.quotesArray = newQuotes;
            [self reloadLabels];
            [self reloadTimeLabel];
        }
    }];
}


#pragma mark - Animations

- (void)animateConstraintChanges {
    if (SYSTEM_VERSION_LESS_THAN(@"9")) {
        [UIView animateWithDuration:NTMAnimationDuration + NTMAnimationDurationStep * 3.f delay:NTMAnimationInitialDelay + NTMAnimationDelayStep * 2.f usingSpringWithDamping:NTMFirstLabelDamping - NTMLabelsDampingStep * 2.f initialSpringVelocity:0 options:0 animations:^{
            [self.quoteTypeLabel layoutIfNeeded];
        } completion:nil];
        
        [UIView animateWithDuration:NTMAnimationDuration + NTMAnimationDurationStep * 2.f delay:NTMAnimationInitialDelay + NTMAnimationDelayStep usingSpringWithDamping:NTMFirstLabelDamping - NTMLabelsDampingStep initialSpringVelocity:0 options:0 animations:^{
            [self.quoteValueLabel layoutIfNeeded];
        } completion:nil];
        
        [UIView animateWithDuration:NTMAnimationDuration + NTMAnimationDurationStep delay:NTMAnimationInitialDelay usingSpringWithDamping:NTMFirstLabelDamping initialSpringVelocity:0 options:0 animations:^{
            [self.differenceLabel layoutIfNeeded];
        } completion:nil];
        
        [UIView animateWithDuration:NTMAnimationDuration delay:0 usingSpringWithDamping:NTMTableViewDamping initialSpringVelocity:0 options:0 animations:^{
            [self.tableView layoutIfNeeded];
        } completion:nil];
    }
    else {
        [UIView animateWithDuration:.2f animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}



#pragma mark - User actions


- (IBAction)menuButtonPressed {
    [self showMenu];
}

- (IBAction)viewTapped:(UITapGestureRecognizer *)sender {
    if (self.tableViewBottomSpaceConstraint.constant == 0) {
        [self hideMenu];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.quotesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;

    NTMQuote *quote = self.quotesArray[row];
    
    NTMCenteredLabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CenteredLabelCell"];
    cell.label.text = [self stringFromQuote:quote];
    
    if (row == self.selectedQuoteIndex) {
        [self makeCellSelected:cell];
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    
    if (self.selectedQuoteIndex != row) {
        NTMCenteredLabelTableViewCell *prevSelectedCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedQuoteIndex inSection:0]];
        [self makeCellDeselected:prevSelectedCell];
        
        NTMCenteredLabelTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self makeCellSelected:cell];
        
        self.selectedQuoteIndex = row;
        [self reloadLabels];
    }
    
    [self hideMenu];
}


#pragma mark - Helpers

- (NSString *)stringFromQuote:(NTMQuote *)quote {
    return [NSString stringWithFormat:@"%@ → %@", quote.firstCurrency.name, quote.secondCurrency.name];
}

- (void)makeCellSelected:(NTMCenteredLabelTableViewCell *)cell {
    cell.label.font = [UIFont fontWithName:@"Lato-Black" size:14.f];
    cell.label.alpha = 1.f;
}

- (void)makeCellDeselected:(NTMCenteredLabelTableViewCell *)cell {
    cell.label.font = [UIFont fontWithName:@"Lato-Regular" size:14.f];
    cell.label.alpha = .7f;
}

- (NSString *)percentDifferenceTextForQuote:(NTMQuote *)quote fromPercent:(double)percent {
    NSString *currencyName = NSLocalizedString(quote.firstCurrency.name, nil);
    
    if (percent == 0) {
        return [NSString stringWithFormat:NSLocalizedString(@"YESTERDAY_DIFFERENCE_TEXT_DID_NOT_CHANGE", nil), currencyName];
    }

    NSString *verb;
    if (percent < 0) {
        verb = NSLocalizedString(@"FELL", nil);
    }
    else {
        verb = NSLocalizedString(@"ROSE", nil);
    }
    
    percent = ABS(percent);
    
    if (percent < 1.0) {
        return [NSString stringWithFormat:NSLocalizedString(@"YESTERDAY_DIFFERENCE_TEXT_LESS_THAN_ONE_PERCENT", nil), currencyName, verb];
    }
    
    NSInteger percentInteger = (NSInteger)round(percent);
    NSInteger remainder = percentInteger % 10;
    NSString *percentWordVariation;
    if (remainder > 4 || (percentInteger > 4 && percentInteger < 21)) {
        percentWordVariation = NSLocalizedString(@"PERCENT3", nil);
    }
    else if (remainder == 1) {
        percentWordVariation = NSLocalizedString(@"PERCENT1", nil);
    }
    else {
        percentWordVariation = NSLocalizedString(@"PERCENT2", nil);
    }
    
    return [NSString stringWithFormat:NSLocalizedString(@"YESTERDAY_DIFFERENCE_TEXT", nil), currencyName, verb, (int)percentInteger, percentWordVariation];
}

- (UIColor *)percentDifferenceTextColorFromPercent:(double)percent {
    return percent < 0 ? [NTMUserInterfaceManager redColor] : [NTMUserInterfaceManager greenColor];
}


#pragma mark - Notifications

- (void)applicationWillEnterForeground {
    [self updateQuotes];
}


#pragma mark - Gesture recognizer delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.tableView]) {
        return NO;
    }
    
    return YES;
}


#pragma mark - Setters/Getters

- (NSArray *)quotesArray {
    if (!_quotesArray) {
        NTMCurrencyType USD = NTMCurrencyTypeUSD;
        NTMCurrencyType eur = NTMCurrencyTypeEuro;
        NTMCurrencyType rub = NTMCurrencyTypeRub;
        
        NTMQuote *quote1 = [NTMQuote unknownQuoteWithCurrencyType:USD andCurrencyType:rub];
        NTMQuote *quote2 = [NTMQuote unknownQuoteWithCurrencyType:USD andCurrencyType:eur];
        NTMQuote *quote3 = [NTMQuote unknownQuoteWithCurrencyType:eur andCurrencyType:rub];
        NTMQuote *quote4 = [NTMQuote unknownQuoteWithCurrencyType:eur andCurrencyType:USD];
        NTMQuote *quote5 = [NTMQuote unknownQuoteWithCurrencyType:rub andCurrencyType:USD];
        NTMQuote *quote6 = [NTMQuote unknownQuoteWithCurrencyType:rub andCurrencyType:eur];

        _quotesArray = @[quote1, quote2, quote3, quote4, quote5, quote6];
    }
    
    return _quotesArray;
}

- (NSDateFormatter *)onlyTimeDateFormatter {
    if (!_onlyTimeDateFormatter) {
        _onlyTimeDateFormatter = [NSDateFormatter new];
        _onlyTimeDateFormatter.dateStyle = NSDateFormatterNoStyle;
        _onlyTimeDateFormatter.timeStyle = NSDateFormatterShortStyle;
    }
    
    return _onlyTimeDateFormatter;
}


@end
