//
//  main.m
//  Quotes
//
//  Created by Tagir Nafikov on 23.07.16.
//
//

#import <UIKit/UIKit.h>
#import "NTMAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NTMAppDelegate class]));
    }
}
