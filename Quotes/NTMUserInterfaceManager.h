//
//  UserInterfaceManager.h
//  MERA-Test
//
//  Created by Tagir Nafikov on 14.07.16.
//
//

#import <Foundation/Foundation.h>

@class UIColor;

@interface NTMUserInterfaceManager : NSObject

+ (instancetype)sharedManager;

+ (UIColor *)redColor;
+ (UIColor *)greenColor;

- (void)showNetworkActivityIndicator;
- (void)hideNetworkActivityIndicator;

@end
