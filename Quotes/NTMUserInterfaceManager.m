//
//  UserInterfaceManager.m
//  MERA-Test
//
//  Created by Tagir Nafikov on 14.07.16.
//
//

#import "NTMUserInterfaceManager.h"
#import <UIKit/UIKit.h>



#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]



@interface NTMUserInterfaceManager()

@property (nonatomic) NSInteger networkActivityCounter;

@end



@implementation NTMUserInterfaceManager


#pragma mark - Singleton

+ (instancetype)sharedManager {
    static NTMUserInterfaceManager *sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [self new];
    });
    
    return sharedManager;
}


#pragma mark - Colors

+ (UIColor *)redColor {
    return UIColorFromRGB(0xCE0000);
}

+ (UIColor *)greenColor {
    return UIColorFromRGB(0x7ED321);
}


#pragma mark - Network activity indicator

- (void)showNetworkActivityIndicator {
    self.networkActivityCounter++;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    });
}

- (void)hideNetworkActivityIndicator {
    self.networkActivityCounter--;
    self.networkActivityCounter = MAX(self.networkActivityCounter, 0);
    
    if (self.networkActivityCounter == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
    }
}


@end
