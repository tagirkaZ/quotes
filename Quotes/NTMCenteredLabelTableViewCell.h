//
//  CenteredLabelTableViewCell.h
//  Quotes
//
//  Created by Tagir Nafikov on 23.07.16.
//
//

#import <UIKit/UIKit.h>

@interface NTMCenteredLabelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
