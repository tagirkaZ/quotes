//
//  Currency.h
//  Quotes
//
//  Created by Tagir Nafikov on 24.07.16.
//
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NTMCurrencyType) {
    NTMCurrencyTypeUSD,
    NTMCurrencyTypeEuro,
    NTMCurrencyTypeRub
};

@interface NTMCurrency : NSObject

@property (nonatomic) NTMCurrencyType type;            // currency type, USD, eur, rub etc
@property (nonatomic) double USDValue;              // USD rate of the currency
@property (nonatomic, readonly) NSString *name;     // name of the currency depending on it's type

- (instancetype)initWithType:(NTMCurrencyType)type;
- (instancetype)initWithType:(NTMCurrencyType)type USDValue:(double)USDValue;

+ (instancetype)currencyWithType:(NTMCurrencyType)type;
+ (instancetype)currencyWithType:(NTMCurrencyType)type USDValue:(double)USDValue;

@end
