//
//  Currency.m
//  Quotes
//
//  Created by Tagir Nafikov on 24.07.16.
//
//

#import "NTMCurrency.h"



@implementation NTMCurrency


#pragma mark - Init

- (instancetype)initWithType:(NTMCurrencyType)type {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _type = type;
    _USDValue = 1.0;
    
    return self;
}

- (instancetype)initWithType:(NTMCurrencyType)type USDValue:(double)USDValue {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _type = type;
    _USDValue = USDValue;
    
    return self;
}

+ (instancetype)currencyWithType:(NTMCurrencyType)type {
    return [[self alloc] initWithType:type];
}

+ (instancetype)currencyWithType:(NTMCurrencyType)type USDValue:(double)USDValue {
    return [[self alloc] initWithType:type USDValue:USDValue];
}


#pragma mark - Setters/Getters

- (NSString *)name {
    switch (self.type) {
        case NTMCurrencyTypeUSD:
            return @"USD";
        case NTMCurrencyTypeEuro:
            return @"EUR";
        case NTMCurrencyTypeRub:
            return @"RUB";
    }
}


@end
