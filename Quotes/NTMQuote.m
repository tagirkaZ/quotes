//
//  Quote.m
//  Quotes
//
//  Created by Tagir Nafikov on 24.07.16.
//
//

#import "NTMQuote.h"



@implementation NTMQuote


#pragma mark - Init

- (instancetype)initWithCurrency:(NTMCurrency *)firstCurrency andCurrency:(NTMCurrency *)secondCurrency {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _firstCurrency = firstCurrency;
    _secondCurrency = secondCurrency;
    _value = secondCurrency.USDValue / firstCurrency.USDValue;
    
    return self;
}

+ (instancetype)quoteWithCurrency:(NTMCurrency *)firstCurrency andCurrency:(NTMCurrency *)secondCurrency {
    return [[self alloc] initWithCurrency:firstCurrency andCurrency:secondCurrency];
}

+ (instancetype)unknownQuoteWithCurrencyType:(NTMCurrencyType)firstCurrencyType andCurrencyType:(NTMCurrencyType)secondCurrencyType {
    NTMCurrency *firstCurrency = [NTMCurrency currencyWithType:firstCurrencyType];
    NTMCurrency *secondCurrency = [NTMCurrency currencyWithType:secondCurrencyType];
    
    NTMQuote *quote = [self quoteWithCurrency:firstCurrency andCurrency:secondCurrency];
    [quote hiddenlySetValue:0];
    return quote;
}


#pragma mark - Hidden setter

- (void)hiddenlySetValue:(double)value {
    _value = value;
}


@end
