//
//  NTMQuotesBuilder.h
//  Quotes
//
//  Created by Tagir Nafikov on 29.07.16.
//
//

#import <Foundation/Foundation.h>
#import "NTMQuote.h"
#import "NTMQuotesDifference.h"

@interface NTMQuotesBuilder : NSObject

- (instancetype)initWithQuotes:(NSDictionary *)quotes yesterdayQuotes:(NSDictionary *)yesterdayQuotes;
+ (instancetype)builderWithQuotes:(NSDictionary *)quotes yesterdayQuotes:(NSDictionary *)yesterdayQuotes;

- (NTMQuote *)quoteForFirstCurrencyType:(NTMCurrencyType)firstCurrencyType secondCurrencyType:(NTMCurrencyType)secondCurrencyType;
- (NTMQuotesDifference *)differenceSinceYesterdayForQuote:(NTMQuote *)quote;

@end
