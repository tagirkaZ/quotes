//
//  Quote.h
//  Quotes
//
//  Created by Tagir Nafikov on 24.07.16.
//
//

#import <Foundation/Foundation.h>
#import "NTMCurrency.h"

@interface NTMQuote : NSObject

@property (nonatomic, readonly) NTMCurrency *firstCurrency;        // first currency of quote's currency pair
@property (nonatomic, readonly) NTMCurrency *secondCurrency;       // second currency of quote's currency pair
@property (nonatomic, readonly) double value;                   // how many units of second currency in one unit of first currency

- (instancetype)initWithCurrency:(NTMCurrency *)firstCurrency andCurrency:(NTMCurrency *)secondCurrency;
+ (instancetype)quoteWithCurrency:(NTMCurrency *)firstCurrency andCurrency:(NTMCurrency *)secondCurrency;
+ (instancetype)unknownQuoteWithCurrencyType:(NTMCurrencyType)firstCurrencyType andCurrencyType:(NTMCurrencyType)secondCurrencyType;  // returns quote with a given currency pair and with unknown value

@end
