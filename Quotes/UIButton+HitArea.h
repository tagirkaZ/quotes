//
//  UIButton+HitArea.h
//  Namaz
//
//  Created by Tagi on 13.02.14.
//
//

#import <UIKit/UIKit.h>

@interface UIButton (HitArea)

@property (nonatomic, assign) UIEdgeInsets hitTestEdgeInsets;

@end
