//
//  QuotesManager.m
//  Quotes
//
//  Created by Tagir Nafikov on 24.07.16.
//
//

#import "NTMQuotesManager.h"
#import "NTMUserInterfaceManager.h"



static const int NTMTiemoutIntervalForRequest = 15;



@interface NTMQuotesManager()

@property (nonatomic) NSMutableDictionary *quotesDict;
@property (nonatomic) NSMutableDictionary *yesterdayQuotesDict;

@property (nonatomic) NSURLSessionConfiguration *sessionConfiguration;
@property (nonatomic) NSDateFormatter *onlyDateDateFormatter;
@property (nonatomic) NTMQuotesBuilder *quotesBuilder;

@end



@implementation NTMQuotesManager


#pragma mark - Singleton

+ (instancetype)sharedManager {
    static NTMQuotesManager *sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [self new];
    });
    
    return sharedManager;
}


#pragma mark - Network

- (void)getQuotesForLastTwoDaysWithCompletion:(void (^)(NSError *, NTMQuotesBuilder *))completion {
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_enter(group);
    __block NSError *errorLatestQuotes;
    [self getQuotesWithCompletion:^(NSError *error) {
        errorLatestQuotes = error;
        dispatch_group_leave(group);
    }];
    
    dispatch_group_enter(group);
    [self getYesterdayQuotesWithCompletion:^(NSError *error) {
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(),^{
        if (errorLatestQuotes) {
            completion(errorLatestQuotes, nil);
        }
        else {
            completion(nil, [NTMQuotesBuilder builderWithQuotes:self.quotesDict yesterdayQuotes:self.yesterdayQuotesDict]);
        }
    });
}

- (void)getQuotesWithCompletion:(void (^)(NSError *))completion {
    [[NTMUserInterfaceManager sharedManager] showNetworkActivityIndicator];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:self.sessionConfiguration];
    NSString *urlString = @"http://apilayer.net/api/live?access_key=7e0a81fa8943a74d1264f87a7f170ef1&currencies=EUR,RUB&format=1";
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [[NTMUserInterfaceManager sharedManager] hideNetworkActivityIndicator];
        
        BOOL success = NO;

        if (!error && data) {
            NSError *error;
            NSDictionary *rootDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            if (!error && [rootDict isKindOfClass:[NSDictionary class]]) {
                NSDictionary *quotesDict = rootDict[@"quotes"];
                
                if ([quotesDict isKindOfClass:[NSDictionary class]]) {
                    id eurQuote = quotesDict[@"USDEUR"];
                    id rubQuote = quotesDict[@"USDRUB"];
                    
                    if ([eurQuote respondsToSelector:@selector(doubleValue)] && [rubQuote respondsToSelector:@selector(doubleValue)]) {
                        self.quotesDict[@(NTMCurrencyTypeEuro)] = @([eurQuote doubleValue]);
                        self.quotesDict[@(NTMCurrencyTypeRub)] = @([rubQuote doubleValue]);
                        
                        success = YES;
                    }
                }
            }
        }
        
        if (success) {
            completion(nil);
        }
        else {
            completion([self networkError]);
        }
    }];
    
    [task resume];
}

- (void)getYesterdayQuotesWithCompletion:(void (^)(NSError *))completion {
    [[NTMUserInterfaceManager sharedManager] showNetworkActivityIndicator];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:self.sessionConfiguration];
    NSString *dateString = [self.onlyDateDateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:-86400]];
    NSString *urlString = [NSString stringWithFormat:@"http://apilayer.net/api/historical?access_key=7e0a81fa8943a74d1264f87a7f170ef1&date=%@&currencies=EUR,RUB&format=1", dateString];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [[NTMUserInterfaceManager sharedManager] hideNetworkActivityIndicator];
        
        BOOL success = NO;
        
        if (!error && data) {
            NSError *error;
            NSDictionary *rootDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            if (!error && [rootDict isKindOfClass:[NSDictionary class]]) {
                NSDictionary *quotesDict = rootDict[@"quotes"];
                
                if ([quotesDict isKindOfClass:[NSDictionary class]]) {
                    id eurQuote = quotesDict[@"USDEUR"];
                    id rubQuote = quotesDict[@"USDRUB"];
                    
                    if ([eurQuote respondsToSelector:@selector(doubleValue)] && [rubQuote respondsToSelector:@selector(doubleValue)]) {
                        self.yesterdayQuotesDict[@(NTMCurrencyTypeEuro)] = @([eurQuote doubleValue]);
                        self.yesterdayQuotesDict[@(NTMCurrencyTypeRub)] = @([rubQuote doubleValue]);
                        
                        success = YES;
                    }
                }
            }
        }
        
        if (success) {
            completion(nil);
        }
        else {
            [self.yesterdayQuotesDict removeObjectForKey:@(NTMCurrencyTypeEuro)];
            [self.yesterdayQuotesDict removeObjectForKey:@(NTMCurrencyTypeRub)];

            completion([self networkError]);
        }
    }];
    
    [task resume];
}


#pragma mark - Helpers

- (NSError *)networkError {
    return [NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey : NSLocalizedString(@"NETWORK_ERROR_TEXT", nil)}];
}


#pragma mark - Settergs/Getters

#pragma mark - Setters/Getters

- (NSMutableDictionary *)quotesDict {
    if (!_quotesDict) {
        _quotesDict = [NSMutableDictionary dictionaryWithDictionary:@{@(NTMCurrencyTypeUSD) : @(1.0)}];
    }
    
    return _quotesDict;
}

- (NSMutableDictionary *)yesterdayQuotesDict {
    if (!_yesterdayQuotesDict) {
        _yesterdayQuotesDict = [NSMutableDictionary dictionaryWithDictionary:@{@(NTMCurrencyTypeUSD) : @(1.0)}];
    }
    
    return _yesterdayQuotesDict;
}

- (NSURLSessionConfiguration *)sessionConfiguration {
    if (!_sessionConfiguration) {
        _sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _sessionConfiguration.URLCache = nil;
        _sessionConfiguration.timeoutIntervalForRequest = NTMTiemoutIntervalForRequest;
    }
    
    return _sessionConfiguration;
}

- (NSDateFormatter *)onlyDateDateFormatter {
    if (!_onlyDateDateFormatter) {
        _onlyDateDateFormatter = [NSDateFormatter new];
        _onlyDateDateFormatter.dateFormat = @"yyyy-MM-dd";
    }
    
    return _onlyDateDateFormatter;
}


@end
