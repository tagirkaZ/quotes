//
//  QuotesManager.h
//  Quotes
//
//  Created by Tagir Nafikov on 24.07.16.
//
//

#import <Foundation/Foundation.h>
#import "NTMQuotesBuilder.h"

@interface NTMQuotesManager : NSObject

+ (instancetype)sharedManager;

- (void)getQuotesForLastTwoDaysWithCompletion:(void (^)(NSError *error, NTMQuotesBuilder *quotesBuilder))completion;

@end
