//
//  NTMQuotesBuilder.m
//  Quotes
//
//  Created by Tagir Nafikov on 29.07.16.
//
//

#import "NTMQuotesBuilder.h"



@interface NTMQuotesBuilder()

@property (nonatomic, copy) NSDictionary *quotesDict;
@property (nonatomic, copy) NSDictionary *yesterdayQuotesDict;

@end



@implementation NTMQuotesBuilder


#pragma mark - Init

- (instancetype)initWithQuotes:(NSDictionary *)quotes yesterdayQuotes:(NSDictionary *)yesterdayQuotes {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _quotesDict = quotes.copy;
    _yesterdayQuotesDict = yesterdayQuotes.copy;
    
    return self;
}

+ (instancetype)builderWithQuotes:(NSDictionary *)quotes yesterdayQuotes:(NSDictionary *)yesterdayQuotes {
    return [[self alloc] initWithQuotes:quotes yesterdayQuotes:yesterdayQuotes];
}


#pragma mark - Quotes

- (NTMQuote *)quoteForFirstCurrencyType:(NTMCurrencyType)firstCurrencyType secondCurrencyType:(NTMCurrencyType)secondCurrencyType {
    id firstCurrencyQuote = self.quotesDict[@(firstCurrencyType)];
    id secondCurrencyQuote = self.quotesDict[@(secondCurrencyType)];
    
    if (!firstCurrencyQuote || !secondCurrencyQuote) {
        return [NTMQuote unknownQuoteWithCurrencyType:firstCurrencyType andCurrencyType:secondCurrencyType];
    }
    
    NTMCurrency *firstCurrency = [NTMCurrency currencyWithType:firstCurrencyType USDValue:[firstCurrencyQuote doubleValue]];
    NTMCurrency *secondCurrency = [NTMCurrency currencyWithType:secondCurrencyType USDValue:[secondCurrencyQuote doubleValue]];
    return [NTMQuote quoteWithCurrency:firstCurrency andCurrency:secondCurrency];
}

- (NTMQuotesDifference *)differenceSinceYesterdayForQuote:(NTMQuote *)quote {
    NTMCurrencyType firstCurrencyType = quote.firstCurrency.type;
    NTMCurrencyType secondCurrencyType = quote.secondCurrency.type;
    id firstCurrencyQuote = self.yesterdayQuotesDict[@(firstCurrencyType)];
    id secondCurrencyQuote = self.yesterdayQuotesDict[@(secondCurrencyType)];
    
    if (!firstCurrencyQuote || !secondCurrencyQuote) {
        return nil;
    }
    
    NTMCurrency *firstCurrency = [NTMCurrency currencyWithType:firstCurrencyType USDValue:[firstCurrencyQuote doubleValue]];
    NTMCurrency *secondCurrency = [NTMCurrency currencyWithType:secondCurrencyType USDValue:[secondCurrencyQuote doubleValue]];
    NTMQuote *yesterdayQuote = [NTMQuote quoteWithCurrency:firstCurrency andCurrency:secondCurrency];
    
    double difference = quote.value - yesterdayQuote.value;
    double percent = difference / (yesterdayQuote.value / 100.0);
    
    return [NTMQuotesDifference differenceWithPercentDifference:percent];
}


@end
