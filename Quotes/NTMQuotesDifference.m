//
//  NTMQuotesDifference.m
//  Quotes
//
//  Created by Tagir Nafikov on 29.07.16.
//
//

#import "NTMQuotesDifference.h"



@implementation NTMQuotesDifference


#pragma mark - Init

- (instancetype)initWithPercentDifference:(double)percentDifference {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _percentDifference = percentDifference;
    
    return self;
}

+ (instancetype)differenceWithPercentDifference:(double)percentDifference {
    return [[self alloc] initWithPercentDifference:percentDifference];
}

@end
