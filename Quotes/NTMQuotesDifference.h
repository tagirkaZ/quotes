//
//  NTMQuotesDifference.h
//  Quotes
//
//  Created by Tagir Nafikov on 29.07.16.
//
//

#import <Foundation/Foundation.h>

@interface NTMQuotesDifference : NSObject

@property (nonatomic, readonly) double percentDifference;

- (instancetype)initWithPercentDifference:(double)percentDifference;
+ (instancetype)differenceWithPercentDifference:(double)percentDifference;

@end
