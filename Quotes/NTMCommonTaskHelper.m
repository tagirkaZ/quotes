//
//  CommonTaskHelper.m
//  Quotes
//
//  Created by Tagir Nafikov on 24.07.16.
//
//

#import "NTMCommonTaskHelper.h"
#import <UIKit/UIKit.h>



@implementation NTMCommonTaskHelper

+ (void)printAllFontNames {
    for (NSString *familyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            NSLog(@"%@", fontName);
        }
    }
}

@end
